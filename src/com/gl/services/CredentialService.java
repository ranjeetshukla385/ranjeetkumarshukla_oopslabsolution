package com.gl.services;

import java.util.Random;

public class CredentialService {
	public String generatePassword() {
		int passwordLength = 8;
		String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
		String specialCharacters = "!@#$%^&*_=+-/.?<>)";

		String numbers = "1234567890";
		String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
		Random random = new Random();
		char[] password = new char[passwordLength];

		password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
		password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
		password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
		password[3] = numbers.charAt(random.nextInt(numbers.length()));

		for (int i = 4; i < passwordLength; i++) {
			password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
		}
		return String.valueOf(password);
	}

	public String generateEmailAddress(String fullName, String department) {

		return fullName + "@" + department + ".abc.com";
	}

	public String showCredentials(String name, String email, String password) {
		String welcomeGreeting = "Dear " + name + " your generated credentials are as follows \nEmail ---> " + email
				+ " \nPassword ---> " + password;
		return welcomeGreeting;
	}
}
