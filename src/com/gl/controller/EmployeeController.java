package com.gl.controller;

import java.util.NoSuchElementException;
import java.util.Scanner;

import com.gl.model.Employee;
import com.gl.services.CredentialService;

public class EmployeeController {

	public static void main(String[] args) {
		Employee employee = new Employee("Ranjeet", "Shukla");
		String fname = employee.getFirstName();
		String lname = employee.getLastName();
		String fullName = fname + lname;

		// Get Department
		String department = "";
		do {
			department = getDepartmentSlug();
			if (department == "")
				System.out.println("Please select the correct department!\n");

		} while (department == "");

		// CredentialService
		CredentialService cs = new CredentialService();
		String pass = cs.generatePassword();
		String email = cs.generateEmailAddress(fullName, department);
		String showCreds = cs.showCredentials(fname, email, pass);
		System.out.println(showCreds);

	}

	/**
	 * This function takes input from user and return department slug
	 * 
	 * @return department slug
	 */
	public static String getDepartmentSlug() throws NoSuchElementException {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Please enter the department from the following \n1. Technical \n2. Admin \n3. Human Resource\n4. Legal \n");
		int departmentIndex = scanner.nextInt();
		switch (departmentIndex) {
		case 1: {
			return "tech";
		}
		case 2: {
			return "admin";
		}
		case 3: {
			return "hr";
		}
		case 4: {
			return "legal";
		}
		default:
			return "";
		}

	}

}
